import { Injectable } from '@angular/core';

function StoreInStorage(config: { prefix: string; storage: Storage }) {
  return function (target: any, propertyKey: string) {
    const key = `${config.prefix}${propertyKey}`;
    Object.defineProperty(target, propertyKey, {
      get: function () {
        return config.storage.getItem(key) || '';
      },
      set: function (v: any) {
        if (v == undefined) {
          config.storage.removeItem(key);
        } else {
          config.storage.setItem(key, v);
        }
      },
    });
  };
}

function Confirm(message: string) {
  return function (
    target: any,
    propertyKey: string,
    propertyDescriptor: PropertyDescriptor
  ) {
    console.log(target, propertyKey, propertyDescriptor);
    return {
      ...propertyDescriptor,
      value: function (...args: any[]) {
        if (confirm(message)) {
          propertyDescriptor.value(...args);
        } else {
          console.log('Function was canceled!');
        }
      },
    } as PropertyDescriptor;
  };
}

@Injectable({ providedIn: 'root' })
export class TestService {
  //   public get testProperty(): string {
  //     return sessionStorage.getItem('testProperty') || '';
  //   }
  //   public set testProperty(v: string) {
  //     if (v == undefined) {
  //       sessionStorage.removeItem('testProperty');
  //     } else {
  //       sessionStorage.setItem('testProperty', v);
  //     }
  //   }

  @StoreInStorage({ prefix: 'TestService.', storage: sessionStorage })
  testProperty?: string;

  @StoreInStorage({ prefix: 'TestService.', storage: localStorage })
  testProperty2?: string;

  @Confirm('Clear storage?')
  clearStorage() {
    localStorage.clear();
    sessionStorage.clear();
    
    // TODO 
    // try {
    //   throw new Error('got three');
    // } catch (ex) {
    //   console.log(ex.message);
    // }
  }
}
