interface IClient {
  id: string;
  name: string;
  age: number;
}

interface IExtendedClient extends IClient {
  customField: any;
}

// interface IClient {
//   customField: any;
// }

const client1: IExtendedClient = {
  age: 1,
  id: '',
  name: '',
  customField: null,
};

type Client = {
  id: string;
  name: string;
  age: number;
};

type ExtendedClient = Client & { customField: any };

type ApplicationManager = Client & { isManager: true };
type ApplicationWorker = Client & { isWorker: true };

type ApplicationUser = ApplicationManager | ApplicationWorker;

const client2: ExtendedClient = {
  age: 1,
  id: '',
  name: '',
  customField: null,
};

const appManager: ApplicationManager = {
  age: 1,
  id: '',
  name: '',
  isManager: true,
};

const appWorker: ApplicationWorker = {
  age: 1,
  id: '',
  name: '',
  isWorker: true,
};

let user: ApplicationUser = appManager;

user = appWorker;

type PartialType<TType> = {
  [Key in keyof TType]?: TType[Key];
};

type Stringified<TType> = {
  [Key in keyof TType]: string;
};

type PartialClient = PartialType<Client>; //Partial<Client>;

function stringify<T>(o: T): Stringified<T> {
  return {} as any;
}

const c: PartialClient = {};

const stringifiedUser = stringify(user);

type ClientKeys = keyof Client;

type ClientWithoutId = Omit<Client, 'id'>;
type ClientKeysWithoutId = Exclude<ClientKeys, 'id'>;

type CustomClient = {
  friends: Client[];
};

type CustomClient1 = {
  friends: Client;
};

type UnArray<T> = T extends Array<infer R> ? R : T;

const a = ['', ''];

type A = {
  [Key in keyof CustomClient]: UnArray<CustomClient[Key]>;
};

function isManager(user: ApplicationUser): user is ApplicationManager {
  return user && (user as any).isManager === true;
}

function processUser(user: ApplicationUser) {
  if (isManager(user)) {
    // TODO manager operations
    user.isManager = true;
  } else {
    // TODO worker opearions
    user.isWorker;
  }
}

class Class1 {
  c1: number = 1; 
}
class Class2 {
  c2: number = 2;
}

declare const cl: unknown;

function isType<T>(o: any): o is T {
    return o;
}

if (isType<Class1>(cl)) {
    cl
} else {
    cl
}

if (cl instanceof Class1) {
    cl
} else {
    cl
}

function isFunction(o: any): o is Function {
    return typeof o === 'function'
}
