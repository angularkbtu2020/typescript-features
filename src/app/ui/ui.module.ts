import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CusomMaterialModule } from './cusom-material/cusom-material.module';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, CusomMaterialModule],
  exports: [CusomMaterialModule, HeaderComponent],
})
export class UiModule {}
